# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  number = rand(1..100)
  loops = 1

  puts "Guess a number"
  guess = gets.chomp.to_i
  until guess == number
    if guess < number
      puts "#{guess} is too low"
    else
      puts "#{guess} is too high"
    end
    puts 'Try again'
    guess = gets.chomp.to_i
    loops += 1
  end

  puts "#{number} was the number! And it only took you #{loops} tries to guess it!"
end

def file_shuffler
  arr = []
  puts 'Which file would you like to shuffle?'
  file_name = gets.chomp
  arr = File.readlines("#{file_name}.txt")
  arr.shuffle!
  File.open("#{filename}-shuffled.txt", "w") do |f|
    arr.each { |el| f.puts el }
  end
end
